The data in "train_data.xlsx" and "test_data.xlsx" is sampled from the
["Airline Twitter sentiment"](https://d1p17r2m4rzlbo.cloudfront.net/wp-content/uploads/2016/03/Airline-Sentiment-2-w-AA.csv) dataset
found at https://www.figure-eight.com/data-for-everyone/ licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

