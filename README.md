# machine-learning

This script gives an idea of how to use PRECIRE as a feature generator.
If you have a text classification problem, you can use the precire API to generate features for
your text samples and use a classification model, e.g. random forests with these features.

## Installation

- python 3 and pip are required
- `pip install -r requirements.txt`

## Usage

- set the `PRECIRE_API_KEY` environment variable to your api key.
- call `python main.py --help` to get a help message.
- input is expected to be in excel format with a `text` and a `label` column.

example call:

```bash
python main.py --train_data data/train_data.xlsx --test_data data/test_data.xlsx
```
