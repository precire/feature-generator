import os

import click
import numpy as np
import pandas as pd
import requests
from tqdm import tqdm

from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline

RESULTS = [
    "competitive",
    "cooperative",
    "directing",
    "innovative",
    "inspiring",
    "professional",
    "venturing",
    "composed",
    "empathic",
    "formal",
    "friendly",
    "goal_oriented",
    "intellectual",
    "motivating",
    "optimistic",
    "positive",
    "reliable",
    "self_confident",
    "structured",
    "supportive",
    "visionary",
]


LABEL_MAP = {"positive": 1, "neutral": 0, "negative": -1}
REVERSE_LABEL_MAP = {1: "positive", 0: "neutral", -1: "negative"}


def precire_results(text, results, use_score, language, api_key):
    """Get precire results for the given text.

    :param text: str with text to be analyzed.
    :param results: list of precire results, that should be returned.
    :param use_score: boolean, if True use the score. If False use percentiles.
    :param language: str, either 'de' or 'en'.
    :param api_key: str, precire api key

    :returns: list of scores/percentiles for each result in results.
    """
    headers = {
        "Ocp-Apim-Subscription-Key": api_key,
        "Content-Type": "application/json",
        "Content-Language": language,
    }

    result_body = [{"name": res, "score": use_score} for res in results]
    body = {"text": {"content": text, "reference": "default"}, "results": result_body}

    # documentation for the response schema can be found here
    # https://precire.ai/documentation-v2#tag/API-Reference
    response = requests.post("https://api.precire.ai/v2", json=body, headers=headers)

    assert response.status_code == 200, (response.status_code, response.json())
    response = response.json()
    value = "score" if use_score else "percentile"
    sample = [response["results"][res][value] for res in results]

    return sample


def precire_features(text, language, api_key):
    """Get precire features for the given text.

    :param text: str with text to be analyzed.
    :param language: str, either 'de' or 'en'.
    :param api_key: str, precire api key

    :returns: list of features for the given text.
    """
    headers = {
        "Ocp-Apim-Subscription-Key": api_key,
        "Content-Type": "application/json",
        "Content-Language": language,
    }

    body = {"document": {"text": text, "type": "default"}}
    # use v1 as v2 does not support features currently
    # documentation for the response schema can be found here
    # https://precire.ai/documentation-v1#operation/v1-features
    response = requests.post("https://api.precire.ai/v1/features", json=body, headers=headers)
    assert response.status_code == 200, (response.status_code, response.json())
    response = response.json()
    return response["features"]


def precire_call(text, results, use_score, use_features, language, api_key):
    """Get precire results or features for the given text.

    :param text: str with text to be analyzed.
    :param results: list of precire results, that should be returned.
    :param use_score: boolean, if True use the score. If False use percentiles.
    :param use_features: boolean, if True use precire features, else precire results.
    :param language: str, either 'de' or 'en'.
    :param api_key: str, precire api key

    :returns: list of results / features for each result in results.
    """
    if use_features:
        return precire_features(text, language, api_key)
    return precire_results(text, results, use_score, language, api_key)


def train(data, results, use_score, use_features, language, api_key):
    """
    :param data: pandas.DataFrame with a text and a label column.
    :param results: list of precire results, that should be used as features.
    :param use_score: boolean, if True use the score. If False use percentiles.
    :param use_features: boolean, if True use precire features, else precire results.
    :param language: str, either 'de' or 'en'.
    :param api_key: str, precire api key

    :returns: a trained model
    """
    features = []
    for i, row in tqdm(data.iterrows(), total=data.shape[0]):
        # get precire features/ results
        sample = precire_call(
            row.text,
            results,
            use_score=use_score,
            use_features=use_features,
            language=language,
            api_key=api_key,
        )

        features.append(sample)
    features = np.array(features)
    targets = data["label"].map(LABEL_MAP)

    pipeline = Pipeline([("PCA", PCA()), ("RF", RandomForestClassifier())])
    parameters = {"PCA__n_components": [2, 8, None], "RF__n_estimators": [1, 5, 10]}
    grid = GridSearchCV(pipeline, param_grid=parameters, cv=5)
    grid.fit(features, targets)
    model = grid.best_estimator_
    print(f"Best score: {grid.best_score_} with params: {grid.best_params_}")
    return model


@click.command()
@click.option(
    "--train_data", help="excel file with training data, must contain 'text' and 'label' columns."
)
@click.option(
    "--test_data", help="excel file with test data, must contain 'text' and 'label' columns."
)
@click.option(
    "--score/--no-score",
    default=True,
    help="defaults to True. If True use score, else percentiles",
)
@click.option(
    "--features/--no-features",
    default=False,
    help="defaults to False. If True use features (v1), else use results (v2)",
)
@click.option("--language", default="en", type=click.Choice(["en", "de"]), help="language to use")
def main(train_data, test_data, score, features, language):
    """train a random forest on precire results or features.
    """
    assert "PRECIRE_API_KEY" in os.environ, "Set PRECIRE_API_KEY environment variable"
    api_key = os.environ["PRECIRE_API_KEY"]
    train_data = pd.read_excel(train_data)
    test_data = pd.read_excel(test_data)
    assert (
        len(set(train_data.text) & set(test_data.text)) == 0
    ), "text from test data also in train data"

    model = train(
        train_data,
        RESULTS,
        use_score=score,
        use_features=features,
        language=language,
        api_key=api_key,
    )

    test_features = []
    for text in test_data.text:
        test_features.append(
            precire_call(
                text,
                RESULTS,
                use_score=score,
                use_features=features,
                language=language,
                api_key=api_key,
            )
        )
    test_features = np.array(test_features)

    predicted = model.predict(test_features)
    print("results for test data:")
    for pred, label, text in zip(predicted, test_data.label, test_data.text):
        print(f"{text} \npredicted: {REVERSE_LABEL_MAP[pred]}, truth: {label}")


if __name__ == "__main__":
    main()
